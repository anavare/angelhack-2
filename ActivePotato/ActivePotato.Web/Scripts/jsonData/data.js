﻿var People = [
	{
	    "id": "1",
	    "image_url": "",
	    "sex": "male",
	    "name": "Richard Parker",
	    "activity_list": [4, 2, 0],
	    "friend_list": [1, 3, 5],
	    "badges": [1, 3],
	    "nickname": "parkeyTiger",
	    "comments": [
				{
				    "text": "Richard is great!"
				},
				{
				    "text": "Had an awesome time"
				}

	    ]
	},
	{
	    "id": "2",
	    "image_url": "",
	    "sex": "male",
	    "name": "Bill Gates",
	    "activity_list": [0, 2, 4],
	    "friend_list": [1, 3, 5],
	    "badges": [3, 5],
	    "nickname": "billy",
	    "comments": [
				{
				    "text": "Bill is great!"
				},
				{
				    "text": "Had an awesome time"
				}
	    ]
	},
	{
	    "id": "3",
	    "image_url": "",
	    "sex": "female",
	    "name": "Katherine Brewster",
	    "activity_list": [2, 1, 4],
	    "friend_list": [1, 3, 5],
	    "badges": [5, 4],
	    "nickname": "Kathy67",
	    "comments": [
				{
				    "text": "Had a great time with Kathy!"
				},
				{
				    "text": "Had an awesome time"
				}
	    ]
	},
	{
	    "id": "4",
	    "image_url": "",
	    "sex": "male",
	    "name": "Max Payne",
	    "activity_list": [2, 0, 3],
	    "friend_list": [1, 3, 5],
	    "badges": [1, 2],
	    "nickname": "maxyPants",
	    "comments": []
	},
	{
	    "id": "5",
	    "image_url": "",
	    "sex": "male",
	    "name": "Steve Jobs",
	    "activity_list": [3, 0, 2],
	    "friend_list": [1, 3, 5],
	    "badges": [3],
	    "nickname": "snobby1",
	    "comments": []
	},
	{
	    "id": "6",
	    "image_url": "",
	    "sex": "male",
	    "name": "Kanye West",
	    "activity_list": [1, 2, 3],
	    "friend_list": [1, 3, 5],
	    "badges": [1],
	    "nickname": "KimmysMan",
	    "comments": []
	},
	{
	    "id": "7",
	    "image_url": "",
	    "sex": "male",
	    "name": "Jon Jones",
	    "activity_list": [0, 3, 1],
	    "friend_list": [1, 3, 5],
	    "badges": [2],
	    "nickname": "ufcchamp",
	    "comments": []
	},
	{
	    "id": "8",
	    "image_url": "",
	    "sex": "male",
	    "name": "Adam Levine",
	    "activity_list": [2, 1, 0],
	    "friend_list": [1, 3, 5],
	    "badges": [],
	    "nickname": "songbird",
	    "comments": []
	},
	{
	    "id": "9",
	    "image_url": "",
	    "sex": "male",
	    "name": "Alan Wake",
	    "activity_list": [0, 1, 2],
	    "friend_list": [1, 3, 5],
	    "badges": [],
	    "nickname": "glowingTorch",
	    "comments": []
	},
	{
	    "id": "10",
	    "image_url": "",
	    "sex": "female",
	    "name": "Holly Madison",
	    "activity_list": [0, 2, 3],
	    "friend_list": [1, 3, 5],
	    "badges": [],
	    "nickname": "hughsgirl",
	    "comments": []
	},
	{
	    "id": "11",
	    "image_url": "",
	    "sex": "female",
	    "name": "Salma Hayek",
	    "activity_list": [2, 0, 3],
	    "friend_list": [1, 3, 5],
	    "badges": [],
	    "nickname": "hot mama",
	    "comments": []
	}
];

var Badges = [
	{
	    "id": "0",
	    "name": "Boagie Chaser"
	},
	{
	    "id": "1",
	    "name": "Barracuda"
	},
	{
	    "id": "2",
	    "name": "Great White"
	},
	{
	    "id": "3",
	    "name": "Bar Fish"
	},
	{
	    "id": "4",
	    "name": "Highlander"
	},
	{
	    "id": "5",
	    "name": "Rocky"
	},
	{
	    "id": "6",
	    "name": "Speedy Gonzales"
	},
	{
	    "id": "7",
	    "name": "High Roller"
	}
];


var Activities = [
    {
    	"id": "0",
    	"name": "Concert",
    	"image_url_cold": "Images/ListActivity/HECK-12.png",
    	"image_url_hot": "Images/ListActivity/HECK-12.png",
    	"valid_badges": [1]
    },
	{
	    "id": "1",
	    "name": "Biking",
	    "image_url_cold": "Images/ListActivity/HECK-13.png",
	    "image_url_hot": "Images/ListActivity/HECK-13.png",
	    "valid_badges": [6]
	},
	{
	    "id": "2",
	    "name": "Competing",
	    "image_url_cold": "Images/ListActivity/HECK-14.png",
	    "image_url_hot": "Images/ListActivity/HECK-16.png",
	    "valid_badges": [7]
	},
	{
	    "id": "3",
	    "name": "Visiting",
	    "image_url_cold": "Images/ListActivity/HECK-15.png",
	    "image_url_hot": "Images/ListActivity/HECK-15.png",
	    "valid_badges": [0]
	},
	{
	    "id": "4",
	    "name": "Coffee",
	    "image_url_cold": "Images/ListActivity/HECK-17.png",
	    "image_url_hot": "Images/ListActivity/HECK-17.png",
	    "valid_badges": [0]
	}
]