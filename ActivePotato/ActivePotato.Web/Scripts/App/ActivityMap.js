﻿$(document).ready(function() {
    $('.headerProfilePic img').click(function() {
        var parentdiv = $(this).attr('parentDiv');

        $(parentdiv).hide();
        $("#profile_container").show();

    });
    $('.headerExploer img').click(function () {
        var parentdiv = $(this).attr('parentDiv');

        $(parentdiv).hide();
        $("#googleMap").show();

    });
    $('.headerCheckin img').click(function () {
        var parentdiv = $(this).attr('parentDiv');

        $(parentdiv).hide();
        $("#divActivityloc").show();

    });

    $('#cycling').click(function () {
        $('#map-canvas').hide();
        $('#map-canvas-coffee').hide();
        $('#map-canvas-competition').hide();
        $('#map-canvas-visiting').hide();
        $('#map-canvas-cycling').show();
        generateCyclingMap();

        $(this).addClass('focus-on');
        $(this).attr('src', '/Images/ActivityMap/cycling-focus.png');
        $('#coffee').removeClass('focus-on');
        $('#coffee').attr('src', '/Images/ActivityMap/coffee.png');
        $('#competition').removeClass('focus-on');
        $('#competition').attr('src', '/Images/ActivityMap/competition.png');
        $('#visiting').removeClass('focus-on');
        $('#visiting').attr('src', '/Images/ActivityMap/visiting.png');
    });

    $('#coffee').click(function () {
        $('#map-canvas').hide();
        $('#map-canvas-cycling').hide();
        $('#map-canvas-competition').hide();
        $('#map-canvas-visiting').hide();
        $('#map-canvas-coffee').show();
        generateCofeeMap();
        
        $(this).addClass('focus-on');
        $(this).attr('src', '/Images/ActivityMap/coffee-focus.png');
        $('#cycling').removeClass('focus-on');
        $('#cycling').attr('src', '/Images/ActivityMap/cycling.png');
        $('#competition').removeClass('focus-on');
        $('#competition').attr('src', '/Images/ActivityMap/competition.png');
        $('#visiting').removeClass('focus-on');
        $('#visiting').attr('src', '/Images/ActivityMap/visiting.png');
    });
    
    $('#competition').click(function () {
        $('#map-canvas').hide();
        $('#map-canvas-cycling').hide();
        $('#map-canvas-coffee').hide();
        $('#map-canvas-visiting').hide();
        $('#map-canvas-competition').show();
        
        generateCompetitionMap();
        
        $(this).addClass('focus-on');
        $(this).attr('src', '/Images/ActivityMap/competition-focus.png');
        $('#cycling').removeClass('focus-on');
        $('#cycling').attr('src', '/Images/ActivityMap/cycling.png');
        $('#coffee').removeClass('focus-on');
        $('#coffee').attr('src', '/Images/ActivityMap/coffee.png');
        $('#visiting').removeClass('focus-on');
        $('#visiting').attr('src', '/Images/ActivityMap/visiting.png');
    });
    
    $('#visiting').click(function () {
        $('#map-canvas').hide();
        $('#map-canvas-cycling').hide();
        $('#map-canvas-coffee').hide();
        $('#map-canvas-competition').hide();
        $('#map-canvas-visiting').show();
        generateVisitingMap();
        
        $(this).addClass('focus-on');
        $(this).attr('src', '/Images/ActivityMap/visit-focus.png');
        $('#cycling').removeClass('focus-on');
        $('#cycling').attr('src', '/Images/ActivityMap/cycling.png');
        $('#coffee').removeClass('focus-on');
        $('#coffee').attr('src', '/Images/ActivityMap/coffee.png');
        $('#competition').removeClass('focus-on');
        $('#competition').attr('src', '/Images/ActivityMap/competition.png');
    });

    $("#mapSwticher").click(function () {
        if ($(this).hasClass("mapSwitcherIcon-on")) {
            $(this).attr("src", "/Images/ActivityMap/CHECKIN-08.png");
            $(this).removeClass("mapSwitcherIcon-on");
            $('#mappedActivities').show();
            $('#activitySpecificPlaces').hide();
        } else {
            $(this).attr("src", "/Images/ActivityMap/CHECKIN-07.png");
            $(this).addClass("mapSwitcherIcon-on");
            $('#mappedActivities').hide();
            $('#activitySpecificPlaces').show();
        }
    });
    generateMap('');

    $('.crossCampusRow').click(function() {
        $("#googleMap").hide();
        $("#UserList").show();
        backTracker.AddLastDiv("#googleMap");
    });
});

var mapOptions = {
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    rotateControl: false,
    panControl: false,
    zoomControl: false,
};

function generateMap(location) {
    var map;

    var locations = [
     ['Cross Campus', 34.017678, -118.490295],
     ['Coloft', 34.019336, -118.488241],
     ['UCLA', 34.074949, -118.441318],
     ['USC', 34.0193721, -118.286109],
     ['Icon Mobile', 34.0132001, -118.49593479999999]
    ];

    var selectedLat = [locations[0][1]];
    var selectedLong = [locations[0][2]];

    mapOptions.center = new google.maps.LatLng(selectedLat, selectedLong),
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            icon: "Images/ActivityMap/map-competition.png",
            map: map
        });
        
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function() {
                $("#googleMap").hide();
                $("#UserList").show();
                
                backTracker.AddLastDiv("#googleMap");
            };
        })(marker, i));
    }
}

function generateCyclingMap() {
    var map;
    mapOptions.center = new google.maps.LatLng(33.663763, -118.010159);
    setZoom();
    map = new google.maps.Map(document.getElementById('map-canvas-cycling'),
        mapOptions);

    
    var locations = [
     ['Cross Campus', 33.75, -118.250145],
     ['Coloft', 33.80, -118.350145],
     ['UCLA', 33.77123, -118.1946],
     ['USC', 33.79223, -118.1946]
    ];

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            icon: "Images/ActivityMap/map-cycling.png",
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                $("#googleMap").hide();
                $("#UserList").show();
                backTracker.AddLastDiv("#googleMap");
            };
        })(marker, i));
    }
}

function generateCofeeMap() {
    var map;
    mapOptions.center = new google.maps.LatLng(33.598666, -117.889266);
    setZoom();
    map = new google.maps.Map(document.getElementById('map-canvas-coffee'),
        mapOptions);

    var locations = [
     ['Cross Campus', 33.798666, -117.889266],
     ['Coloft', 33.398666, -117.689266],
     ['UCLA', 33.46893, -117.55963],
     ['USC', 33.55236, -117.63654]
    ];

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            icon: "Images/ActivityMap/map-coffee.png",
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                $("#googleMap").hide();
                $("#UserList").show();
                backTracker.AddLastDiv("#googleMap");
            };
        })(marker, i));
    }
}

function generateCompetitionMap() {
    var map;

    var locations = [
     ['Cross Campus', 34.017678, -118.490295],
     ['Coloft', 34.019336, -118.488241],
     ['UCLA', 34.074949, -118.441318],
     ['USC', 34.0193721, -118.286109],
     ['Icon Mobile', 34.0132001, -118.49593479999999]
    ];

    var selectedLat = [locations[0][1]];
    var selectedLong = [locations[0][2]];

    mapOptions.center = new google.maps.LatLng(selectedLat, selectedLong),
    map = new google.maps.Map(document.getElementById('map-canvas-competition'), mapOptions);

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            icon: "Images/ActivityMap/map-competition.png",
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                $("#googleMap").hide();
                $("#UserList").show();
                backTracker.AddLastDiv("#googleMap");
            };
        })(marker, i));
    }
}

function generateVisitingMap() {
    var map;
    mapOptions.center = new google.maps.LatLng(33.196025, -117.381620);
    setZoom();
    map = new google.maps.Map(document.getElementById('map-canvas-visiting'),
        mapOptions);

    var locations = [
     ['Cross Campus', 34.017678, -118.490295],
     ['Coloft', 34.019336, -118.488241],
     ['UCLA', 34.074949, -118.441318],
     ['USC', 34.0193721, -118.286109]
    ];

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            icon: "Images/ActivityMap/map-visiting.png",
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                $("#googleMap").hide();
                $("#UserList").show();
                backTracker.AddLastDiv("#googleMap");
            };
        })(marker, i));
    }
}

function setZoom() {
    mapOptions.zoom = 10;
}
