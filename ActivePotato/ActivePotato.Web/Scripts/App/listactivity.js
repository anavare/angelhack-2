﻿$(document).ready(function () {

    $("#divSingUp").hide();

    $("#divFlashScreen").click(function () {
        $("#divFlashScreen").hide();
        $("#divSingUp").show();
    });

    $("#btnNext").click(function () {
        $("#divSingUp").hide();
        $("#divCreateInterest").show();
    });

    $("#btnFinish").click(function () {
        $("#divCreateInterest").hide();
        $("#divActivityList").show();
    });

    $("#divInterestBiking").click(function() {
        $(this).attr("src","/Images/Archive/HACKATHON2-96.png");
    });
    
    $("#divInterestCompeting").click(function() {
        $(this).attr("src","/Images/Archive/HACKATHON2-94.png");
    });
    

    $("#divCreateInterest").hide();
    $("#divActivityList").hide();
    var activityList = $("#activitylist");
    $.each(Activities, function (index, value) {
        var coldImage = '<img class="w1" src="' + value.image_url_cold + '"/>';
        var hotImage = '<img class="w2" src="' + value.image_url_hot + '"/>';
        var item = '<li id="' + value.name + '" class="activityListItem">' + coldImage + hotImage + '</li>';
        activityList.append(item);
    });

    $(".w2").hide();
    $("#divActivityInfo").hide();
    $("#divPlotComment").hide();
    $("#divActivityloc").hide();
    $("#googleMap").hide();
    $("#profile_container").hide();
    $("#match_container").hide();
    $("#UserList").hide();
    
    $(".w1").mouseover(function () {
        var li = $(this).closest('li');
        $(li).find('.w1').hide();
        $(li).find('.w2').show();
    });
    
    $(".w2").mouseleave(function () {
        var li = $(this).closest('li');
        $(li).find('.w2').hide();
        $(li).find('.w1').show();
    });
    
    $(".w2").click(function () {
        $("#divActivityList").hide();
        $("#divActivityInfo").show();
        $("#divPlotComment").show('slow');
        $("#lblActivityLoc").hide();

        backTracker.AddLastDiv("#divActivityList");
    });

    $("#chooseActivityLoc").click(function () {
        $("#divActivityInfo").hide();
        $("#divActivityloc").show();
        
        backTracker.AddLastDiv("#divActivityInfo");
    });
    
    $("#btnPlot").mouseover(function () {
        $(this).attr("src", "Images/ListActivity/HECK-21.png");
    });
    
    $("#btnPlot").mouseleave(function () {
        $(this).attr("src", "Images/ListActivity/HECK-20.png");
    });

    $("#btnPlot").click(function() {
        $("#divActivityInfo").hide();
        $("#googleMap").show();
        mapOptions.zoom = 20;
        generateMap();
        backTracker.AddLastDiv("#divActivityInfo");
    });
    
    $("#activityloclist>li").click(function () {
        var selectedItem = $(this).find(".labelMargin").html();
        $("#divActivityInfo").show();
        $("#divActivityloc").hide();

        backTracker.AddLastDiv("#divActivityloc");
        
        $("#imgActivityLoc").attr("src", "Images/ListActivity/HECK-26_withName.png");
        $("#lblActivityLoc").show();
        $("#lblActivityLoc").text("@ " + selectedItem);
    });

    $("#myProfile").click(function() {
        $("#googleMap").hide();
        $("#profile_container").show();
        
        backTracker.AddLastDiv("#googleMap");
    });

    $("#myMatches").click(function () {
        $("#googleMap").hide();
        $("#match_container").show();
        
        backTracker.AddLastDiv("#googleMap");
    });

    $("#linktoconnectBox").click(function () {
        $("#UserList").hide();
        $("#match_container").show();
        
        backTracker.AddLastDiv("#UserList");
    });

    $("#btnConnectConnection").click(function () {
        $("#match_container").hide();
        $("#profile_container").show();
        
        backTracker.AddLastDiv("#match_container");
    });

    $(".backButton").click(function() {
        var lastDiv = backTracker.GetLastDiv();
        $(this).closest(".divPanel").hide();
        $(lastDiv).show();
    });
    

});

var navigation = (function() {
    var divs = new Array("Saab", "Volvo", "BMW")

})();

var backTracker = (function () {
    var divs = new Array();
    function addLastDiv(divName) {
        divs.push(divName);
    }

    function getLastDiv(divName) {
        return divs.pop();
    }

    return {
        AddLastDiv: addLastDiv,
        GetLastDiv: getLastDiv
    };
})();
