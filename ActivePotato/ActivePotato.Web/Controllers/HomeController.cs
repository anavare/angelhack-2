﻿using System.Web.Mvc;

namespace ActivePotato.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            //return View("ListActivity");
            return View();
        }
        
        public ActionResult ShowProfile()
        {
            return View("Profile");
        }

        public ActionResult ActivityMap()
        {
            ViewBag.Message = "Map of Activity selected.";

            return View("ActivityMap");

        }

        public ActionResult ProfileMatch()
        {
            return View();
        }


        public ActionResult UserList()
        {
            return View();
        }
    }
}
